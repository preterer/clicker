import EmberObject from "@ember/object";

const Automatic = EmberObject.extend({
  name: "",
  step: 0,
  price: 0,
  unit: "second",
  unitTimeInMs: 1000,

  init() {
    this.interval = setInterval(
      () => this.increment(),
      Automatic.TIME_PER_STEP
    );
  },

  increment() {
    this.counter.increment(
      this.step * (Automatic.TIME_PER_STEP / this.unitTimeInMs)
    );
  },
  cancel() {
    clearInterval(this.interval);
  }
});

Automatic.reopenClass({
  TIME_PER_STEP: 100, // ms
  isAutomatic(object) {
    return object && object.name && object.step && object.price;
  },

  shopElement(name, step, price) {
    return { name, step, price, unit: "second" };
  }
});

export default Automatic;
