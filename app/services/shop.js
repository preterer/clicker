import Service from "@ember/service";
import Automatic from "../models/automatic";
import { inject as service } from "@ember/service";

export default Service.extend({
  STORAGE_KEY: "clicker_automatics",
  counter: service("counter"),
  automatics: undefined,
  availableAutomatics: undefined,

  buy(automatic) {
    if (
      Automatic.isAutomatic(automatic) &&
      this.counter.count >= automatic.price
    ) {
      const { name, step, price } = automatic;
      this.automatics.pushObject(
        Automatic.create({ name, step, price, counter: this.counter })
      );
      this.counter.decrease(automatic.price);
      this.saveAutomatics();
    }
  },

  init() {
    this._super(...arguments);
    this.loadAutomatics();
    this.generateShopEntries(50);
  },

  loadAutomatics() {
    const storageData = localStorage[this.STORAGE_KEY];
    const parsedData = storageData ? JSON.parse(storageData) : [];
    const automatics = parsedData.map(automatic => {
      automatic.counter = this.counter;
      return Automatic.create(automatic);
    });
    this.set("automatics", automatics);
  },

  saveAutomatics() {
    localStorage[this.STORAGE_KEY] = JSON.stringify(this.automatics);
  },

  reset() {
    localStorage.removeItem(this.STORAGE_KEY);
    this.automatics.forEach(automatic => automatic.cancel());
    this.loadAutomatics();
  },

  generateShopEntries(amount) {
    this.availableAutomatics = new Array(amount)
      .fill(undefined)
      .map((_, index) =>
        Automatic.shopElement(
          `Level ${index + 1}`,
          this.generateStep(index + 1),
          this.generateCost(index + 1)
        )
      );
  },

  generateStep(index) {
    return index * index - index || 1;
  },

  generateCost(index) {
    return index * index * 10;
  }
});
