import Service from "@ember/service";

export default Service.extend({
  STORAGE_KEY: "clicker_counter",
  count: 0,

  init() {
    this._super(...arguments);
    this.loadCount();
  },

  increment(step) {
    if (step) {
      this.set("count", this.count + step);
      this.saveCount();
    }
  },

  decrease(amount) {
    if (amount) {
      this.set("count", this.count - amount);
      this.saveCount();
    }
  },

  loadCount() {
    const storedValue = localStorage[this.STORAGE_KEY];
    this.set("count", storedValue ? JSON.parse(storedValue) : 0);
  },

  saveCount() {
    localStorage[this.STORAGE_KEY] = this.count;
  },

  reset() {
    localStorage.removeItem(this.STORAGE_KEY);
    this.loadCount();
  }
});
