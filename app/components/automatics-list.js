import Component from "@ember/component";
import { computed } from "@ember/object";

export default Component.extend({
  groups: computed("automatics.[]", function() {
    const groups = [];
    this.automatics.forEach(automatic => {
      const index = groups.findIndex(group => group.name === automatic.name);
      if (index > -1) {
        const group = groups[index];
        group.total += automatic.step;
        group.count++;
      } else {
        groups.push({
          name: automatic.name,
          total: automatic.step,
          count: 1,
          unit: automatic.unit
        });
      }
    });
    return groups;
  })
});
