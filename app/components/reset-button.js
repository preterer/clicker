import Component from "@ember/component";
import { inject as service } from "@ember/service";

export default Component.extend({
  counter: service(),
  shop: service(),

  actions: {
    reset() {
      this.counter.reset();
      this.shop.reset();
    }
  }
});
