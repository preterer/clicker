import Route from "@ember/routing/route";
import { inject as service } from "@ember/service";

export default Route.extend({
  shop: service(),

  model() {
    return this.shop;
  },

  actions: {
    buy(automatic) {
      this.shop.buy(automatic);
    }
  }
});
