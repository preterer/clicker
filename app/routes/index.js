import Route from "@ember/routing/route";
import { inject as service } from "@ember/service";

export default Route.extend({
  counter: service(),
  shop: service(),

  model() {
    return {
      counter: this.counter,
      shop: this.shop
    };
  }
});
