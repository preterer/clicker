import { helper } from "@ember/component/helper";

export function formatFloat(params) {
  return params[0] && params[0].toFixed(params[1] || 0);
}

export default helper(formatFloat);
