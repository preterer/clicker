import Component from "@ember/component";
import { inject as service } from "@ember/service";

export default Component.extend({
  counter: service(),

  actions: {
    click() {
      this.counter.increment(1);
    }
  }
});
