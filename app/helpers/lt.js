import { helper } from "@ember/component/helper";

export function lt([numberA, numberB]) {
  return !isNaN(numberA) && !isNaN(numberB) && numberA < numberB;
}

export default helper(lt);
